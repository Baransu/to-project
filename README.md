# java-blog-service

## Running the service

### Building

```sh
mvn clean package
```

### Creating docker image

```sh
./mvnw install dockerfile:build
```

### Running docker

```sh
# run postgres
docker run -p 5432:5432 --rm --name postgres -d postgres:9.6.9
# run service
docker run -p 8080:8080 cichocinski/java-blog-service:{current version}
```

## Design patterns

- Builder
- Repository
- Dependency Injection
- Command
- DTO

## Technologies

- Java 8
- PostgreSQL
- Spring
- Spring Boot
- Lombok
- JUnit
- Mockito

## Authors

- Tomasz Cichociński
