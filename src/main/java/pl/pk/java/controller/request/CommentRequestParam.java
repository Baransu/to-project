package pl.pk.java.controller.request;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class CommentRequestParam {
  private Long userId;
  private String content;
}
