package pl.pk.java.controller.request;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.pk.java.model.Post;
import pl.pk.java.model.User;

import javax.validation.constraints.NotBlank;
import java.time.ZonedDateTime;
import java.util.Date;

@Getter
@JsonRootName("post")
@NoArgsConstructor
public class PostRequestParam {
  @NotBlank(message = "can't be empty")
  private String title;

  @NotBlank(message = "can't be empty")
  private String content;

  private Date date;

  private PostRequestParam(String title, String content) {
    this.title = title;
    this.content = content;
    this.date = Date.from(ZonedDateTime.now().toInstant());
  }

  public static Builder newBuilder() {
    return new Builder();
  }

  public Post toPost(User user) {
    return new Post(user, title, content, date);
  }

  @NoArgsConstructor
  public static class Builder {
    private String title;
    private String content;

    public Builder title(String title) {
      this.title = title;
      return this;
    }

    public Builder content(String content) {
      this.content = content;
      return this;
    }

    public PostRequestParam build() {
      return new PostRequestParam(this.title, this.content);
    }
  }
}
