package pl.pk.java.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pk.java.commands.DownVoteCommand;
import pl.pk.java.commands.UpVoteCommand;
import pl.pk.java.model.Comment;
import pl.pk.java.service.CommentService;

import javax.validation.Valid;

@RestController
public class CommentController {

  private CommentService commentService;

  public CommentController(CommentService commentService) {
    this.commentService = commentService;
  }

  @PutMapping("/comments/{commentId/upvote")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Comment upvote(@PathVariable Long commentId, @Valid @RequestBody int amount) {

    return commentService.vote(commentId, new UpVoteCommand(amount));
  }

  @PutMapping("/comments/{commentId/downvote")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Comment downvote(@PathVariable Long commentId, @Valid @RequestBody int amount) {

    return commentService.vote(commentId, new DownVoteCommand(amount));
  }
}
