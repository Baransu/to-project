package pl.pk.java.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.pk.java.controller.request.CommentRequestParam;
import pl.pk.java.controller.request.PostRequestParam;
import pl.pk.java.model.Comment;
import pl.pk.java.model.Post;
import pl.pk.java.model.PostDto;
import pl.pk.java.service.CommentService;
import pl.pk.java.service.PostService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class PostsController {

  private PostService postService;
  private CommentService commentService;

  public PostsController(PostService postService, CommentService commentService) {
    this.postService = postService;
    this.commentService = commentService;
  }

  @GetMapping("/posts")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<PostDto> getAll() {
    List<Post> posts = postService.getAll();
    return posts.stream().map(this::convertToDto).collect(Collectors.toList());
  }

  @GetMapping("/posts/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public PostDto get(@PathVariable Long id) {
    Post post = postService.getById(id);
    return convertToDto(post);
  }

  @PostMapping("/users/{userId}/posts")
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public PostDto create(
      @PathVariable Long userId, @Valid @RequestBody PostRequestParam newPostParam) {
    Post post = postService.create(userId, newPostParam);
    return convertToDto(post);
  }

  @GetMapping("/users/{userId}/posts")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<PostDto> getByUserId(@PathVariable Long userId) {
    List<Post> posts = postService.getByUserId(userId);
    return posts.stream().map(this::convertToDto).collect(Collectors.toList());
  }

  @GetMapping("/posts/{postId}/comments")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public List<Comment> getCommentsByPostId(@PathVariable Long postId) {
    return commentService.getByPostId(postId);
  }

  @PostMapping("/posts/{postId}/comments")
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public Comment create(
      @PathVariable Long postId, @Valid @RequestBody CommentRequestParam commentRequestParam) {

    return commentService.create(postId, commentRequestParam);
  }

  private PostDto convertToDto(Post post) {
    PostDto postDto = new PostDto(post.getId(), post.getTitle(), post.getContent(), post.getUser());

    postDto.setSubmissionDate(post.getDate(), "UTC");

    return postDto;
  }
}
