package pl.pk.java.service;

import org.springframework.stereotype.Service;
import pl.pk.java.controller.request.PostRequestParam;
import pl.pk.java.exception.ResourceNotFoundException;
import pl.pk.java.model.Post;
import pl.pk.java.repository.PostRepository;
import pl.pk.java.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

  private final PostRepository postRepository;

  private final UserRepository userRepository;

  public PostServiceImpl(PostRepository postRepository, UserRepository userRepository) {
    this.postRepository = postRepository;
    this.userRepository = userRepository;
  }

  @Override
  public List<Post> getAll() {
    return postRepository.findAll();
  }

  @Override
  public Post getById(Long id) {
    Optional<Post> post = postRepository.findById(id);

    if (!post.isPresent()) {
      throw new ResourceNotFoundException();
    }

    return post.get();
  }

  @Override
  public Post create(Long userId, PostRequestParam postRequestParam) {
    return userRepository
        .findById(userId)
        .map(user -> postRepository.save(postRequestParam.toPost(user)))
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public List<Post> getByUserId(Long userId) {
    return postRepository.findByUserId(userId);
  }
}
