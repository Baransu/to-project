package pl.pk.java.service;

import org.springframework.stereotype.Service;
import pl.pk.java.exception.ResourceNotFoundException;
import pl.pk.java.model.User;
import pl.pk.java.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public List<User> getAllUsers() {
    return userRepository.findAll();
  }

  @Override
  public User getUser(Long id) {
    Optional<User> user = userRepository.findById(id);

    if (!user.isPresent()) {
      throw new ResourceNotFoundException();
    }

    return user.get();
  }

  @Override
  public User createUser(User newUser) {
    return userRepository.save(newUser);
  }
}
