package pl.pk.java.service;

import pl.pk.java.controller.request.PostRequestParam;
import pl.pk.java.model.Post;

import java.util.List;

public interface PostService {
  Post getById(Long id);

  List<Post> getByUserId(Long userId);

  List<Post> getAll();

  Post create(Long userId, PostRequestParam postRequestParam);
}
