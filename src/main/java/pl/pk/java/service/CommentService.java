package pl.pk.java.service;

import pl.pk.java.commands.CommentCommand;
import pl.pk.java.controller.request.CommentRequestParam;
import pl.pk.java.model.Comment;

import java.util.List;

public interface CommentService {
  Comment create(Long postId, CommentRequestParam commentRequestParam);

  List<Comment> getByPostId(Long postId);

  Comment vote(Long commentId, CommentCommand command);
}
