package pl.pk.java.service;

import org.springframework.stereotype.Service;
import pl.pk.java.commands.CommentCommand;
import pl.pk.java.controller.request.CommentRequestParam;
import pl.pk.java.exception.ResourceNotFoundException;
import pl.pk.java.model.Comment;
import pl.pk.java.model.Post;
import pl.pk.java.model.User;
import pl.pk.java.repository.CommentRepository;
import pl.pk.java.repository.PostRepository;
import pl.pk.java.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {

  private CommentRepository commentRepository;

  private PostRepository postRepository;

  private UserRepository userRepository;

  public CommentServiceImpl(
      PostRepository postRepository,
      UserRepository userRepository,
      CommentRepository commentRepository) {
    this.postRepository = postRepository;
    this.userRepository = userRepository;
    this.commentRepository = commentRepository;
  }

  @Override
  public Comment create(Long postId, CommentRequestParam commentRequestParam) {
    Long userId = commentRequestParam.getUserId();

    Optional<User> user = userRepository.findById(userId);

    if (!user.isPresent()) {
      throw new ResourceNotFoundException();
    }

    Optional<Post> post = postRepository.findById(postId);

    if (!post.isPresent()) {
      throw new ResourceNotFoundException();
    }

    Comment comment = new Comment(post.get(), user.get(), commentRequestParam.getContent());

    return commentRepository.save(comment);
  }

  @Override
  public List<Comment> getByPostId(Long postId) {
    return commentRepository.findByPostId(postId);
  }

  @Override
  public Comment vote(Long commentId, CommentCommand command) {

    Optional<Comment> maybeComment = commentRepository.findById(commentId);

    if (!maybeComment.isPresent()) {
      throw new ResourceNotFoundException();
    }

    Comment comment = maybeComment.get();
    command.execute(comment);

    return commentRepository.save(comment);
  }
}
