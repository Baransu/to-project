package pl.pk.java.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pk.java.model.Comment;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

  List<Comment> findByUserId(Long userId);

  List<Comment> findByPostId(Long postId);
}
