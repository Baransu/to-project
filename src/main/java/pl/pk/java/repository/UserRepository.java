package pl.pk.java.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pk.java.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {}
