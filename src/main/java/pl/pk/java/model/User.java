package pl.pk.java.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

@Getter
@NoArgsConstructor
@javax.persistence.Entity
@Table(name = "USERS")
public class User extends pl.pk.java.model.Entity {

  private String firstName;
  private String lastName;

  public User(String firstName, String lastName) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
