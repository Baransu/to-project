package pl.pk.java.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@NoArgsConstructor
@Getter
@javax.persistence.Entity
@Table(name = "COMMENTS")
public class Comment extends Entity {

  @ManyToOne(fetch = FetchType.EAGER, optional = false)
  @JoinColumn(name = "user_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private User user;

  @ManyToOne(fetch = FetchType.EAGER, optional = false)
  @JoinColumn(name = "post_id", nullable = false)
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Post post;

  private int votes;

  private String content;

  public Comment(Post post, User user, String content) {
    super();
    this.votes = 0;
    this.post = post;
    this.user = user;
    this.content = content;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void upVoteBy(int amount) {
    this.votes = this.votes + amount;
  }

  public void downVoteBy(int amount) {
    this.votes = this.votes - amount;
  }
}
