package pl.pk.java.commands;

import pl.pk.java.model.Comment;

public class UpVoteCommand implements CommentCommand {
  private int amount;

  public UpVoteCommand(int amount) {
    this.amount = amount;
  }

  @Override
  public void execute(Comment comment) {
    comment.upVoteBy(amount);
  }
}
