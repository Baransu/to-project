package pl.pk.java.commands;

import pl.pk.java.model.Comment;

public interface CommentCommand {
  void execute(Comment comment);
}
