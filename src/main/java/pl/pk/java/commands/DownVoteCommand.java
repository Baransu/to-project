package pl.pk.java.commands;

import pl.pk.java.model.Comment;

public class DownVoteCommand implements CommentCommand {
  private int amount;

  public DownVoteCommand(int amount) {
    this.amount = amount;
  }

  @Override
  public void execute(Comment comment) {
    comment.downVoteBy(amount);
  }
}
