package pl.pk.java;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.pk.java.controller.request.PostRequestParam;
import pl.pk.java.controller.request.UserRequestParam;
import pl.pk.java.model.Post;
import pl.pk.java.model.User;
import pl.pk.java.repository.PostRepository;
import pl.pk.java.service.PostServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTest {

  @Mock private PostRepository postRepository;

  @InjectMocks private PostServiceImpl postService;

  private Post post;
  private User user;

  @Before
  public void setup() {
    user = UserRequestParam.newBuilder().firstName("John").lastName("Doe").build().toUser();
    post = PostRequestParam.newBuilder().title("title").content("lorem ipsum").build().toPost(user);
  }

  @Test
  public void getOne() {

    // when
    when(postRepository.findById(1L)).thenReturn(Optional.of(post));

    // then
    Post body = postService.getById(1L);

    assertEquals(post, body);
  }

  @Test
  public void getAll() {
    // when
    List<Post> posts = new ArrayList<>();
    posts.add(post);
    when(postRepository.findAll()).thenReturn(posts);

    List<Post> body = postService.getAll();

    // Then
    assertEquals(posts, body);
  }

  @Test
  public void getByUserId() {
    // when
    List<Post> posts = new ArrayList<>();
    posts.add(post);
    when(postRepository.findByUserId(user.getId())).thenReturn(posts);

    // then
    List<Post> body = postService.getByUserId(user.getId());

    assertEquals(posts, body);
  }
}
