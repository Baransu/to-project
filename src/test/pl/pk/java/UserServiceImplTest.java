package pl.pk.java;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.pk.java.controller.request.UserRequestParam;
import pl.pk.java.model.User;
import pl.pk.java.repository.UserRepository;
import pl.pk.java.service.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

  @Mock private UserRepository userRepository;

  @InjectMocks private UserServiceImpl userService;

  private User user;

  @Before
  public void setup() {
    user = UserRequestParam.newBuilder().firstName("John").lastName("Doe").build().toUser();
  }

  @Test
  public void create() {

    // when
    when(userRepository.save(any(User.class))).thenReturn(user);

    // then
    User body = userService.createUser(user);

    assertEquals(user.getFirstName(), body.getFirstName());
    assertEquals(user.getLastName(), body.getLastName());
  }

  @Test
  public void getOne() {

    // when
    when(userRepository.findById(1L)).thenReturn(Optional.of(user));

    // then
    User body = userService.getUser(1L);

    assertEquals(user, body);
  }

  @Test
  public void getAll() {
    // when
    List<User> users = new ArrayList<>();
    users.add(user);
    when(userRepository.findAll()).thenReturn(users);

    List<User> body = userService.getAllUsers();

    // Then
    assertEquals(users, body);
  }
}
