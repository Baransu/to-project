package pl.pk.java;

import org.junit.Before;
import org.junit.Test;
import pl.pk.java.commands.CommentCommand;
import pl.pk.java.commands.DownVoteCommand;
import pl.pk.java.commands.UpVoteCommand;
import pl.pk.java.model.Comment;
import pl.pk.java.model.Post;
import pl.pk.java.model.User;

import java.time.ZonedDateTime;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class CommentServiceImplTest {

  private Comment comment;

  @Before
  public void setup() {
    User user = new User("firstName", "lastName");
    Date date = Date.from(ZonedDateTime.now().toInstant());
    Post post = new Post(user, "content", "title", date);
    comment = new Comment(post, user, "content");
  }

  @Test
  public void voting() {
    CommentCommand upvote = new UpVoteCommand(10);
    CommentCommand downvote = new DownVoteCommand(5);

    upvote.execute(comment);
    downvote.execute(comment);

    assertEquals(comment.getVotes(), 5);
  }
}
